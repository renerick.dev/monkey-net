﻿using System;
using System.Linq;
using Monkey.Net.Parsing.Lexing;
using Monkey.Net.Parsing.Parsing;

namespace Monkey.Net.Repl
{
    internal static class Program
    {
        private const string Prompt = "$$>";

        static void Main(string[] args)
        {
            Run();
        }

        static void Run()
        {
            while (true)
            {
                Console.Write(Prompt);
                var input = Console.ReadLine();
                var l = new Lexer(input);
                var p = new Parser(l);
                var prog = p.Parse();
                if (p.Errors.Any())
                {
                    foreach (var parserError in p.Errors)
                    {
                        Console.WriteLine($"   {parserError.Message}");
                    }
                }
                // else
                {
                    Console.WriteLine(prog.ToString());
                }
            }
        }
    }
}
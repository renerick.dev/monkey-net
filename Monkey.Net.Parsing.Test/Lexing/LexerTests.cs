﻿using System.Collections.Generic;
using FluentAssertions;
using KellermanSoftware.CompareNetObjects;
using Monkey.Net.Parsing.Lexing;
using Xunit;

namespace Monkey.Net.Parsing.Test.Lexing
{
    public class LexerTests
    {
        public static IEnumerable<object[]> TestCases = new[]
        {
            new object[]
            {
                "= + - * / ( ) { } > < ! , ;", new Token[]
                {
                    new(TokenType.Assign, "="),
                    new(TokenType.Plus, "+"),
                    new(TokenType.Minus, "-"),
                    new(TokenType.Asterisk, "*"),
                    new(TokenType.Slash, "/"),
                    new(TokenType.LParenthesis, "("),
                    new(TokenType.RParenthesis, ")"),
                    new(TokenType.LBrace, "{"),
                    new(TokenType.RBrace, "}"),
                    new(TokenType.GT, ">"),
                    new(TokenType.LT, "<"),
                    new(TokenType.Bang, "!"),
                    new(TokenType.Comma, ","),
                    new(TokenType.Semicolon, ";"),
                    new(TokenType.EOF, "EOF"),
                },
            },
            new object[]
            {
                "== != <= >=", new Token[]
                {
                    new(TokenType.Equals, "=="),
                    new(TokenType.NotEquals, "!="),
                    new(TokenType.LE, "<="),
                    new(TokenType.GE, ">="),
                    new(TokenType.EOF, "EOF"),
                },
            },
            new object[]
            {
                "let fn return if else true false", new Token[]
                {
                    new(TokenType.Let, "let"),
                    new(TokenType.Function, "fn"),
                    new(TokenType.Return, "return"),
                    new(TokenType.If, "if"),
                    new(TokenType.Else, "else"),
                    new(TokenType.True, "true"),
                    new(TokenType.False, "false"),
                    new(TokenType.EOF, "EOF"),
                },
            },
            new object[]
            {
                "a 1 10 11 _1 ab _abd ab1 0a12", new Token[]
                {
                    new(TokenType.Identifier, "a"),
                    new(TokenType.Integer, "1"),
                    new(TokenType.Integer, "10"),
                    new(TokenType.Integer, "11"),
                    new(TokenType.Identifier, "_1"),
                    new(TokenType.Identifier, "ab"),
                    new(TokenType.Identifier, "_abd"),
                    new(TokenType.Identifier, "ab1"),
                    new(TokenType.Illegal, "0a12"),
                    new(TokenType.EOF, "EOF"),
                    new(TokenType.EOF, "EOF"),
                },
            },
            new object[]
            {
                "let a = (10 + b_) >= 0 == true; return a;", new Token[]
                {
                    new(TokenType.Let, "let"),
                    new(TokenType.Identifier, "a"),
                    new(TokenType.Assign, "="),
                    new(TokenType.LParenthesis, "("),
                    new(TokenType.Integer, "10"),
                    new(TokenType.Plus, "+"),
                    new(TokenType.Identifier, "b_"),
                    new(TokenType.RParenthesis, ")"),
                    new(TokenType.GE, ">="),
                    new(TokenType.Integer, "0"),
                    new(TokenType.Equals, "=="),
                    new(TokenType.True, "true"),
                    new(TokenType.Semicolon, ";"),
                    new(TokenType.Return, "return"),
                    new(TokenType.Identifier, "a"),
                    new(TokenType.Semicolon, ";"),
                    new(TokenType.EOF, "EOF"),
                    new(TokenType.EOF, "EOF"),
                },
            },
            new object[]
            {
                "_10) 10)", new Token[]
                {
                    new(TokenType.Identifier, "_10"),
                    new(TokenType.RParenthesis, ")"),
                    new(TokenType.Integer, "10"),
                    new(TokenType.RParenthesis, ")"),
                    new(TokenType.EOF, "EOF"),
                },
            },
        };

        [Fact]
        public void Lexer_FromEmptyString_EmitsEOF()
        {
            var l = new Lexer("");
            var t = l.NextToken();

            t.Type.Should().Be(TokenType.EOF);
            t.Literal.Should().Be("EOF");
        }

        [Theory]
        [MemberData(nameof(TestCases))]
        public void Lexer_FromInputString_EmitsCorrectSequenceOfTokens(string input, Token[] expectedTokens)
        {
            var l = new Lexer(input);

            foreach (var testCaseExpectedToken in expectedTokens)
            {
                var t = l.NextToken();
                var compare = new CompareLogic();

                var diff = compare.Compare(t, testCaseExpectedToken);
                diff.Differences.Should().BeEmpty();
            }
        }
    }
}
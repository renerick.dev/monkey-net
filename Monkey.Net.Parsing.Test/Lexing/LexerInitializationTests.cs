﻿using FluentAssertions;
using Monkey.Net.Parsing.Lexing;
using Xunit;

namespace Monkey.Net.Parsing.Test.Lexing
{
    public class LexerInitializationTests
    {
        [Fact]
        public void Lexer_IsCorrectly_Initialized()
        {
            const string input = "let a = 10;";
            var l = new Lexer(input);

            l.Input.Should().Be(input);
            l.CurrentPosition.Should().Be(0);
            l.PeekPosition.Should().Be(1);
            l.CurrentCharacter.Should().Be('l');
            l.PeekCharacter.Should().Be('e');
        }

        [Fact]
        public void Lexer_IsCorrectlyInitialized_WithOnlyOneCharacter()
        {
            const string input = "a";
            var l = new Lexer(input);

            l.Input.Should().Be(input);
            l.CurrentPosition.Should().Be(0);
            l.PeekPosition.Should().Be(1);
            l.CurrentCharacter.Should().Be('a');
            l.PeekCharacter.Should().Be('\0');
        }

        [Fact]
        public void Lexer_IsCorrectlyInitialized_WithEmptyString()
        {
            var l = new Lexer("");

            l.Input.Should().Be(string.Empty);
            l.CurrentPosition.Should().Be(0);
            l.PeekPosition.Should().Be(0);
            l.CurrentPosition.Should().Be('\0');
            l.PeekPosition.Should().Be('\0');
        }
    }
}
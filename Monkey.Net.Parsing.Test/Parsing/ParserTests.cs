﻿using System.Collections.Generic;
using FluentAssertions;
using KellermanSoftware.CompareNetObjects;
using Monkey.Net.Parsing.Ast;
using Monkey.Net.Parsing.Ast.Expressions;
using Monkey.Net.Parsing.Ast.Statements;
using Monkey.Net.Parsing.Lexing;
using Monkey.Net.Parsing.Parsing;
using Xunit;

namespace Monkey.Net.Parsing.Test.Parsing
{
    public class ParserTests
    {
        public static IEnumerable<object[]> TestCases = new[]
        {
            new object[]
            {
                "return a;",
                "return a;",
                new Program(new IStatement[]
                    {
                        new ReturnStatement(new Token(TokenType.Return, "return"),
                            new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"))
                    }
                ),
            },
            new object[]
            {
                "a;",
                "a;",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"))
                    }
                ),
            },
            new object[]
            {
                "a + b;",
                "(a + b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.Plus, "+"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a * b;",
                "(a * b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.Asterisk, "*"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a - b;",
                "(a - b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.Minus, "-"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a / b;",
                "(a / b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.Slash, "/"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a == b;",
                "(a == b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.Equals, "=="),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a != b;",
                "(a != b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.NotEquals, "!="),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a > b;",
                "(a > b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.GT, ">"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a >= b;",
                "(a >= b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.GE, ">="),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a < b;",
                "(a < b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.LT, "<"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },
            new object[]
            {
                "a <= b;",
                "(a <= b);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.LE, "<="),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b"))
                        )
                    }
                ),
            },

            new object[]
            {
                "a * b / c + 10 - 1 > 4 == true",
                "((((((a * b) / c) + 10) - 1) > 4) == true);",
                new Program(new IStatement[]
                    {
                        new ExpressionStatement(new Token(TokenType.Identifier, "a"),
                            new InfixOperatorExpression(new Token(TokenType.Equals, "=="),
                                new InfixOperatorExpression(new Token(TokenType.GT, ">"),
                                    new InfixOperatorExpression(new Token(TokenType.Minus, "-"),
                                        new InfixOperatorExpression(new Token(TokenType.Plus, "+"),
                                            new InfixOperatorExpression(new Token(TokenType.Slash, "/"),
                                                new InfixOperatorExpression(new Token(TokenType.Asterisk, "*"),
                                                    new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                                    new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b")),
                                                new IdentifierExpression(new Token(TokenType.Identifier, "c"), "c")),
                                            new IntegerExpression(new Token(TokenType.Integer, "10"), 10)),
                                        new IntegerExpression(new Token(TokenType.Integer, "1"), 1)),
                                    new IntegerExpression(new Token(TokenType.Integer, "4"), 4)),
                                new BooleanExpression(new Token(TokenType.True, "true"), true))
                        ),
                    }
                ),
            },
            new object[]
            {
                "true == 4 <= -1 + 10 + a * b / c",
                "(true == (4 <= (((-1) + 10) + ((a * b) / c))));",
                new Program(new IStatement[]
                {
                    new ExpressionStatement(new Token(TokenType.True, "true"),
                        new InfixOperatorExpression(new Token(TokenType.Equals, "=="),
                            new BooleanExpression(new Token(TokenType.True, "true"), true),
                            new InfixOperatorExpression(new Token(TokenType.LE, "<="),
                                new IntegerExpression(new Token(TokenType.Integer, "4"), 4),
                                new InfixOperatorExpression(new Token(TokenType.Plus, "+"),
                                    new InfixOperatorExpression(new Token(TokenType.Plus, "+"),
                                        new PrefixOperatorExpression(new Token(TokenType.Minus, "-"), new IntegerExpression(new Token(TokenType.Integer, "1"), 1)),
                                        new IntegerExpression(new Token(TokenType.Integer, "10"), 10)
                                    ),
                                    new InfixOperatorExpression(new Token(TokenType.Slash, "/"),
                                        new InfixOperatorExpression(new Token(TokenType.Asterisk, "*"),
                                            new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                            new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b")
                                        ),
                                        new IdentifierExpression(new Token(TokenType.Identifier, "c"), "c")
                                    )
                                )
                            )
                        )
                    ),
                }),
            },
            new object[]
            {
                "(a + b) * c",
                "((a + b) * c);",
                new Program(new IStatement[]
                {
                    new ExpressionStatement(new Token(TokenType.LParenthesis, "("),
                        new InfixOperatorExpression(new Token(TokenType.Asterisk, "*"),
                            new InfixOperatorExpression(new Token(TokenType.Plus, "+"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"),
                                new IdentifierExpression(new Token(TokenType.Identifier, "b"), "b")
                            ),
                            new IdentifierExpression(new Token(TokenType.Identifier, "c"), "c")
                        )
                    )
                }),
            },
            new object[]
            {
                "fn(a) {return a;}",
                "fn(a) { return a; };",
                new Program(new IStatement[]
                {
                    new ExpressionStatement(new Token(TokenType.Function, "fn"),
                        new FunctionDeclarationExpression(new Token(TokenType.Function, "fn"),
                            new List<IdentifierExpression>()
                            {
                                new(new Token(TokenType.Identifier, "a"), "a"),
                            },
                            new BlockStatement(
                                new[]
                                {
                                    new ReturnStatement(new Token(TokenType.Return, "return"), new IdentifierExpression(new Token(TokenType.Identifier, "a"), "a"))
                                }
                            )
                        )
                    ),
                }),
            },
        };

        [Theory]
        [MemberData(nameof(TestCases))]
        public void TestAstParsing(string input, string expectedString, INode expectedAst)
        {
            var l = new Lexer(input);
            var p = new Parser(l);
            var prog = p.Parse();
            prog.ToString().Should().Be(expectedString);
            var compare = new CompareLogic();
            var diff = compare.Compare(prog, expectedAst);
            diff.Differences.Should().BeEmpty();
        }
    }
}
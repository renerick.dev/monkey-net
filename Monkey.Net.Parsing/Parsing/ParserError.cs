﻿namespace Monkey.Net.Parsing.Parsing
{
    public class ParserError
    {
        public string Message { get; }

        public ParserError(string message)
        {
            Message = message;
        }
    }
}
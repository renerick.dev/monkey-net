﻿using System.Collections.Generic;
using Monkey.Net.Parsing.Ast;
using Monkey.Net.Parsing.Ast.Expressions;
using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Parsing
{
    internal enum OperatorPrecedence
    {
        Lowest,
        Equals,
        Comparison,
        Sum,
        Product,
        Prefix,
        Call,
    }

    public partial class Parser
    {
        private delegate IExpression? PrefixParser();

        private delegate IExpression? InfixParser(IExpression left);

        private static readonly Dictionary<TokenType, OperatorPrecedence> OperatorPrecedences = new()
        {
            { TokenType.Equals, OperatorPrecedence.Equals },
            { TokenType.NotEquals, OperatorPrecedence.Equals },
            { TokenType.LT, OperatorPrecedence.Comparison },
            { TokenType.LE, OperatorPrecedence.Comparison },
            { TokenType.GT, OperatorPrecedence.Comparison },
            { TokenType.GE, OperatorPrecedence.Comparison },
            { TokenType.Plus, OperatorPrecedence.Sum },
            { TokenType.Minus, OperatorPrecedence.Sum },
            { TokenType.Asterisk, OperatorPrecedence.Product },
            { TokenType.Slash, OperatorPrecedence.Product },
        };

        private readonly Dictionary<TokenType, PrefixParser> _prefixParsers;
        private readonly Dictionary<TokenType, InfixParser> _infixParsers;

        private IExpression? ParseInfixOperatorExpression(IExpression left)
        {
            var ct = _currentToken;
            MoveNext();
            var right = ParseExpression(LookupPrecedence(ct.Type));
            return right == null ? null : new InfixOperatorExpression(ct, left, right);
        }


        private IExpression? ParseExpression(OperatorPrecedence precedence)
        {
            if (CurrentTokenIs(TokenType.EOF))
            {
                AddError("Unexpected EOF");
                return null;
            }

            var ct = _currentToken;

            if (!_prefixParsers.TryGetValue(ct.Type, out var prefixParser))
            {
                AddError($"Unknown operator {ct.Literal}");
                return null;
            }

            var left = prefixParser();

            if (left == null)
            {
                return null;
            }

            while (!PeekTokenIs(TokenType.Semicolon) && precedence < LookupPrecedence(_peekToken.Type))
            {
                if (!_infixParsers.TryGetValue(_peekToken.Type, out var infixParser))
                {
                    return left;
                }

                MoveNext();

                left = infixParser(left);

                if (left == null)
                {
                    return null;
                }
            }

            return left;
        }

        private static OperatorPrecedence LookupPrecedence(TokenType currentTokenType)
        {
            return OperatorPrecedences.TryGetValue(currentTokenType, out var precedence)
                ? precedence
                : OperatorPrecedence.Lowest;
        }

        private IExpression ParseIdentifierExpression()
        {
            return new IdentifierExpression(_currentToken, _currentToken.Literal);
        }

        private IExpression ParseBooleanExpression()
        {
            var ct = _currentToken;
            var value = bool.Parse(ct.Literal);
            return new BooleanExpression(ct, value);
        }

        private IExpression ParseIntegerExpression()
        {
            var ct = _currentToken;
            var value = long.Parse(ct.Literal);
            return new IntegerExpression(ct, value);
        }

        private IExpression? ParsePrefixOperatorExpression()
        {
            var ct = _currentToken;
            MoveNext();
            var expr = ParseExpression(OperatorPrecedence.Prefix);
            return expr == null ? null : new PrefixOperatorExpression(ct, expr);
        }

        private IExpression? ParseGroupedExpression()
        {
            MoveNext();
            var expr = ParseExpression(OperatorPrecedence.Lowest);
            ExpectNextToken(TokenType.RParenthesis);
            return expr;
        }

        private IExpression? ParseFunctionDeclaration()
        {
            var ct = _currentToken;
            if (!ExpectNextToken(TokenType.LParenthesis))
            {
                return null;
            }

            var parameters = ParseParameters();
            if (parameters == null)
            {
                return null;
            }

            if (!ExpectNextToken(TokenType.LBrace))
            {
                return null;
            }

            var body = ParseBlockStatement();

            return new FunctionDeclarationExpression(ct, parameters, body);
        }

        private BlockStatement ParseBlockStatement()
        {
            MoveNext();
            var result = new BlockStatement();
            while (!CurrentTokenIs(TokenType.EOF) && !CurrentTokenIs(TokenType.RBrace))
            {
                var statement = ParseStatement();
                if (statement != null)
                    result.Statements.AddLast(statement);
                MoveNext();
            }

            return result;
        }

        private List<IdentifierExpression>? ParseParameters()
        {
            var result = new List<IdentifierExpression>();
            MoveNext();
            while (!CurrentTokenIs(TokenType.RParenthesis))
            {
                var expr = (IdentifierExpression) ParseIdentifierExpression();
                result.Add(expr);
                if (!PeekTokenIs(TokenType.RParenthesis))
                {
                    if (!ExpectNextToken(TokenType.Comma))
                    {
                        return null;
                    }
                }

                MoveNext();
            }

            return result;
        }
    }
}
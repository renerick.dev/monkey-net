﻿using System.Collections.Generic;
using Monkey.Net.Parsing.Ast;
using Monkey.Net.Parsing.Ast.Statements;
using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Parsing
{
    public partial class Parser
    {
        private readonly Lexer _lexer;
        private Token _currentToken;
        private Token _peekToken;

        private readonly List<ParserError> _errors;

        public IReadOnlyList<ParserError> Errors => _errors;

        public Parser(Lexer lexer)
        {
            _lexer = lexer;
            _errors = new List<ParserError>();
            _currentToken = default;
            _peekToken = default;
            _prefixParsers = new Dictionary<TokenType, PrefixParser>()
            {
                { TokenType.Identifier, ParseIdentifierExpression },
                { TokenType.Integer, ParseIntegerExpression },
                { TokenType.True, ParseBooleanExpression },
                { TokenType.False, ParseBooleanExpression },
                { TokenType.Minus, ParsePrefixOperatorExpression },
                { TokenType.Bang, ParsePrefixOperatorExpression },
                { TokenType.LParenthesis, ParseGroupedExpression },
                { TokenType.Function, ParseFunctionDeclaration }
            };

            _infixParsers = new Dictionary<TokenType, InfixParser>()
            {
                { TokenType.Plus, ParseInfixOperatorExpression },
                { TokenType.Minus, ParseInfixOperatorExpression },
                { TokenType.Asterisk, ParseInfixOperatorExpression },
                { TokenType.Slash, ParseInfixOperatorExpression },
                { TokenType.Equals, ParseInfixOperatorExpression },
                { TokenType.NotEquals, ParseInfixOperatorExpression },
                { TokenType.LT, ParseInfixOperatorExpression },
                { TokenType.LE, ParseInfixOperatorExpression },
                { TokenType.GT, ParseInfixOperatorExpression },
                { TokenType.GE, ParseInfixOperatorExpression },
            };
            MoveNext();
            MoveNext();
        }

        public Program Parse()
        {
            var program = new Program();

            while (!CurrentTokenIs(TokenType.EOF))
            {
                var statement = ParseStatement();
                if (statement != null)
                    program.Statements.AddLast(statement);
                MoveNext();
            }

            return program;
        }

        private void MoveNext()
        {
            _currentToken = _peekToken;
            _peekToken = _lexer.NextToken();
        }

        private IStatement? ParseStatement()
        {
            return _currentToken.Type switch
            {
                TokenType.Semicolon => null,
                // TokenType.Let => ParseLetStatement(),
                TokenType.Return => ParseReturnStatement(),
                _ => ParseExpressionStatement(),
            };
        }

        private ReturnStatement? ParseReturnStatement()
        {
            var ct = _currentToken;
            MoveNext();
            var expr = ParseExpression(OperatorPrecedence.Lowest);
            if (expr == null) return null;

            if (!ExpectNextToken(TokenType.Semicolon)) return null;

            return new ReturnStatement(ct, expr);
        }

        private ExpressionStatement? ParseExpressionStatement()
        {
            var ct = _currentToken;
            var expr = ParseExpression(OperatorPrecedence.Lowest);
            return expr == null ? null : new ExpressionStatement(ct, expr);
        }

        private bool CurrentTokenIs(TokenType type) => _currentToken.Type == type;
        private bool PeekTokenIs(TokenType type) => _peekToken.Type == type;

        private void AddError(string message)
        {
            _errors.Add(new ParserError(message));
        }

        private bool ExpectNextToken(TokenType type)
        {
            if (PeekTokenIs(type))
            {
                MoveNext();
                return true;
            }

            AddError($"Expected {type}, got {_currentToken.Literal}");
            return false;
        }
    }
}
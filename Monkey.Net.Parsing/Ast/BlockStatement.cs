﻿using System.Collections.Generic;
using System.Linq;

namespace Monkey.Net.Parsing.Ast
{
    public class BlockStatement : INode
    {
        public BlockStatement()
        {
            Statements = new LinkedList<IStatement>();
        }

        public BlockStatement(IEnumerable<IStatement> statements)
        {
            Statements = new LinkedList<IStatement>(statements);
        }

        public LinkedList<IStatement> Statements { get; }

        public override string ToString()
        {
            return string.Join("", Statements.Select(s => s.ToString()));
        }

        public string TokenLiteral => "";
    }
}
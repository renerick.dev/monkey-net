﻿using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Ast.Expressions
{
    public class IntegerExpression : IExpression
    {
        private readonly Token _token;

        public IntegerExpression(Token token, long value)
        {
            _token = token;
            Value = value;
        }

        public long Value { get; }

        public string TokenLiteral => _token.Literal;

        public override string ToString() => TokenLiteral;
    }
}
﻿using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Ast.Expressions
{
    public class IdentifierExpression : IExpression
    {
        public string Name { get; }

        private readonly Token _token;

        public IdentifierExpression(Token token, string name)
        {
            Name = name;
            _token = token;
        }

        public string TokenLiteral => _token.Literal;

        public override string ToString() => TokenLiteral;
    }
}
﻿using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Ast.Expressions
{
    public class BooleanExpression : IExpression
    {
        private readonly Token _token;

        public bool Value { get; }

        public BooleanExpression(Token token, bool value)
        {
            _token = token;
            Value = value;
        }

        public string TokenLiteral => _token.Literal;

        public override string ToString() => TokenLiteral;
    }
}
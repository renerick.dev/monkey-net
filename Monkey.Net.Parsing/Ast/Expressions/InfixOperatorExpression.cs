﻿using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Ast.Expressions
{
    public class InfixOperatorExpression : IExpression
    {
        private readonly Token _token;

        public IExpression Left { get; }

        public IExpression Right { get; }

        public InfixOperatorExpression(Token token, IExpression left, IExpression right)
        {
            _token = token;
            Left = left;
            Right = right;
        }

        public string TokenLiteral => _token.Literal;

        public override string ToString() => $"({Left} {TokenLiteral} {Right})";
    }
}
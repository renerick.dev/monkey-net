﻿using System.Collections.Generic;
using System.Linq;
using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Ast.Expressions
{
    public class FunctionDeclarationExpression : IExpression
    {
        private Token _token;

        public FunctionDeclarationExpression(Token token, List<IdentifierExpression> parameters, BlockStatement body)
        {
            _token = token;
            Parameters = parameters;
            Body = body;
        }

        public string TokenLiteral
        {
            get => _token.Literal;
        }

        public override string ToString()
        {
            return $"{TokenLiteral}({string.Join(", ", Parameters.Select(p => p.ToString()))}) {{ {Body} }}";
        }

        public List<IdentifierExpression> Parameters { get; set; }

        public BlockStatement Body { get; set; }
    }
}
﻿using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Ast.Expressions
{
    public class PrefixOperatorExpression : IExpression
    {
        private readonly Token _token;

        public PrefixOperatorExpression(Token token, IExpression expression)
        {
            _token = token;
            Expression = expression;
        }

        public IExpression Expression { get; }

        public string TokenLiteral => _token.Literal;

        public override string ToString()
        {
            return $"({TokenLiteral}{Expression})";
        }
    }
}
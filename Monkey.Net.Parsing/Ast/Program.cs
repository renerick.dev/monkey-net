﻿using System.Collections.Generic;
using System.Linq;

namespace Monkey.Net.Parsing.Ast
{
    public class Program : INode
    {
        public Program()
        {
            Statements = new LinkedList<IStatement>();
        }

        public Program(IEnumerable<IStatement> statements)
        {
            Statements = new LinkedList<IStatement>(statements);
        }

        public LinkedList<IStatement> Statements { get; }

        public override string ToString()
        {
            return string.Join("", Statements.Select(s => s.ToString()));
        }

        public string TokenLiteral => "";
    }
}
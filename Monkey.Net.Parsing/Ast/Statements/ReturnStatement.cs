﻿using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Ast.Statements
{
    public class ReturnStatement : IStatement
    {
        private readonly Token _token;

        public ReturnStatement(Token token, IExpression value)
        {
            _token = token;
            Value = value;
        }

        public IExpression Value { get; }

        public string TokenLiteral => _token.Literal;

        public override string ToString()
        {
            return $"{TokenLiteral} {Value};";
        }
    }
}
﻿using Monkey.Net.Parsing.Lexing;

namespace Monkey.Net.Parsing.Ast.Statements
{
    public class ExpressionStatement : IStatement
    {
        private Token _token;

        public IExpression Expression { get; }

        public ExpressionStatement(Token token, IExpression expression)
        {
            _token = token;
            Expression = expression;
        }

        public string TokenLiteral => _token.Literal;

        public override string ToString() => Expression + ";";
    }
}
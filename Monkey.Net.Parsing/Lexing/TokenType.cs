﻿namespace Monkey.Net.Parsing.Lexing
{
    public enum TokenType
    {
        EOF,
        Illegal,

        // Separators
        Semicolon,
        Comma,

        // Values (i.e. identifiers and literals)
        Identifier,
        Integer,

        True,
        False,

        // Keywords
        Let,
        Function,
        Return,
        If,
        Else,

        // Operators
        Assign,
        Plus,
        Minus,
        Asterisk,
        Slash,
        Bang,

        Equals,
        NotEquals,
        LT,
        GT,
        LE,
        GE,

        // Brackets
        LParenthesis,
        RParenthesis,
        LBrace,
        RBrace,
    }
}
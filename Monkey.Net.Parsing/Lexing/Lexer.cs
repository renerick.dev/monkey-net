﻿namespace Monkey.Net.Parsing.Lexing
{
    public class Lexer
    {
        public Lexer(string input)
        {
            Input = input;
            CurrentPosition = 0;
            PeekPosition = 0;

            MoveNext();
        }

        public string Input { get; }

        public int CurrentPosition { get; private set; }

        public int PeekPosition { get; private set; }

        public char CurrentCharacter => CurrentPosition >= Input.Length ? '\0' : Input[CurrentPosition];

        public char PeekCharacter => PeekPosition >= Input.Length ? '\0' : Input[PeekPosition];

        // ReSharper disable once CyclomaticComplexity
        public Token NextToken()
        {
            SkipWhiteSpace();

            Token result = CurrentCharacter switch
            {
                '=' => ReadTokensWithEqSign(),
                '+' => new Token(TokenType.Plus, CurrentCharacter),
                '-' => new Token(TokenType.Minus, CurrentCharacter),
                '*' => new Token(TokenType.Asterisk, CurrentCharacter),
                '/' => new Token(TokenType.Slash, CurrentCharacter),
                ';' => new Token(TokenType.Semicolon, CurrentCharacter),
                ',' => new Token(TokenType.Comma, CurrentCharacter),
                '!' => ReadTokenWithBang(),
                '<' => ReadTokenWithLeftAngleBracket(),
                '>' => ReadTokenWithRightAngleBracket(),
                '(' => new Token(TokenType.LParenthesis, CurrentCharacter),
                ')' => new Token(TokenType.RParenthesis, CurrentCharacter),
                '{' => new Token(TokenType.LBrace, CurrentCharacter),
                '}' => new Token(TokenType.RBrace, CurrentCharacter),
                '\0' => new Token(TokenType.EOF, "EOF"),
                _ => ReadIdentifierOrKeyword(),
            };

            MoveNext();
            return result;
        }

        private Token ReadIdentifierOrKeyword()
        {
            Token result;
            if (Syntax.IsLetter(CurrentCharacter))
                result = ReadKeywordOrIdentifier();
            else if (Syntax.IsDigit(CurrentCharacter))
                result = ReadNumberLiteral();
            else
                result = new Token(TokenType.Illegal, CurrentCharacter);
            return result;
        }

        private Token ReadTokenWithRightAngleBracket()
        {
            Token result;
            if (PeekCharacter == '=')
            {
                result = new Token(TokenType.GE, CurrentCharacter.ToString() + PeekCharacter);
                MoveNext();
            }
            else
            {
                result = new Token(TokenType.GT, CurrentCharacter);
            }

            return result;
        }

        private Token ReadTokenWithLeftAngleBracket()
        {
            Token result;
            if (PeekCharacter == '=')
            {
                result = new Token(TokenType.LE, CurrentCharacter.ToString() + PeekCharacter);
                MoveNext();
            }
            else
            {
                result = new Token(TokenType.LT, CurrentCharacter);
            }

            return result;
        }

        private Token ReadTokenWithBang()
        {
            Token result;
            if (PeekCharacter == '=')
            {
                result = new Token(TokenType.NotEquals, CurrentCharacter.ToString() + PeekCharacter);
                MoveNext();
            }
            else
            {
                result = new Token(TokenType.Bang, CurrentCharacter);
            }

            return result;
        }

        private Token ReadTokensWithEqSign()
        {
            Token result;
            if (PeekCharacter == '=')
            {
                result = new Token(TokenType.Equals, CurrentCharacter.ToString() + PeekCharacter);
                MoveNext();
            }
            else
            {
                result = new Token(TokenType.Assign, CurrentCharacter);
            }

            return result;
        }

        private void MoveNext()
        {
            if (CurrentCharacter == '\0') return;

            CurrentPosition = PeekPosition;
            PeekPosition += 1;
        }

        private void SkipWhiteSpace()
        {
            while (Syntax.IsWhiteSpace(CurrentCharacter))
                MoveNext();
        }

        private Token ReadKeywordOrIdentifier()
        {
            var startPos = CurrentPosition;
            while (Syntax.IsLetter(PeekCharacter) || Syntax.IsDigit(PeekCharacter))
                MoveNext();

            var literal = Input[startPos..(CurrentPosition + 1)];

            return new Token(Syntax.LookupKeywords(literal), literal);
        }

        private Token ReadNumberLiteral()
        {
            var startPos = CurrentPosition;
            var tokenType = TokenType.Integer;
            while (Syntax.IsDigit(PeekCharacter) || Syntax.IsLetter(PeekCharacter))
            {
                if (Syntax.IsLetter(CurrentCharacter)) tokenType = TokenType.Illegal;
                MoveNext();
            }

            var literal = Input[startPos..(CurrentPosition + 1)];

            return new Token(tokenType, literal);
        }
    }
}
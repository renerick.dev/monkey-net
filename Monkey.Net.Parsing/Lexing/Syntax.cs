﻿using System.Collections.Generic;

namespace Monkey.Net.Parsing.Lexing
{
    public static class Syntax
    {
        private static readonly Dictionary<string, TokenType> Keywords = new()
        {
            { "return", TokenType.Return },
            { "let", TokenType.Let },
            { "fn", TokenType.Function },
            { "if", TokenType.If },
            { "else", TokenType.Else },
            { "true", TokenType.True },
            { "false", TokenType.False },
        };

        public static bool IsWhiteSpace(char ch)
        {
            return ch is ' ' or '\n' or '\r' or '\t';
        }

        public static bool IsLetter(char ch)
        {
            return char.IsLetter(ch) || ch == '_';
        }

        public static bool IsDigit(char ch)
        {
            return char.IsNumber(ch);
        }

        public static TokenType LookupKeywords(string literal)
        {
            return Keywords.TryGetValue(literal, out var type)
                ? type
                : TokenType.Identifier;
        }
    }
}
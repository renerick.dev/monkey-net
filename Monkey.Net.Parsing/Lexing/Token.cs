﻿using System.Diagnostics;

namespace Monkey.Net.Parsing.Lexing
{
    [DebuggerDisplay("{Type} {Literal}")]
    public struct Token
    {
        public Token(TokenType type, string literal)
        {
            Type = type;
            Literal = literal;
        }

        public Token(TokenType type, char literal)
        {
            Type = type;
            Literal = literal.ToString();
        }

        public TokenType Type { get; set; }

        public string Literal { get; set; }
    }
}